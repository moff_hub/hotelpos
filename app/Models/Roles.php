<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class, 'role_users');
    }
}
